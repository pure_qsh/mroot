/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.event


import org.quartz.Scheduler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import wang.encoding.mroot.admin.common.quartz.QuartzScheduleUtil
import wang.encoding.mroot.model.entity.system.ScheduleJob
import wang.encoding.mroot.model.enums.ScheduleJobOperationTypeEnum


/**
 * 事件监听器
 *
 * @author ErYang
 */
@Component
class AdminEventListener {


    @Autowired
    private lateinit var scheduler: Scheduler


    /**
     * 定时任务 监听规则事件
     *
     * @param scheduleJobOperationEvent ScheduleJobOperationEvent
     * @throws Exception Exception
     */
    @Async
    @EventListener
    @Throws(Exception::class)
    fun scheduleJobOperationEvent(scheduleJobOperationEvent: ScheduleJobOperationEvent) {
        // 类型
        val type: ScheduleJobOperationTypeEnum? = scheduleJobOperationEvent.scheduleJobOperationTypeEnum
        if (null != type) {
            // 定时任务
            var scheduleJob: ScheduleJob? = null

            val any: Any = scheduleJobOperationEvent.source
            if (any is ScheduleJob) {
                scheduleJob = any
            }
            if (null != scheduleJob) {
                when (type) {
                    ScheduleJobOperationTypeEnum.ADD -> // 新增
                        QuartzScheduleUtil.addScheduleJob(scheduler, scheduleJob)
                    ScheduleJobOperationTypeEnum.UPDATE -> // 编辑
                        QuartzScheduleUtil.editScheduleJob(scheduler, scheduleJob)
                    ScheduleJobOperationTypeEnum.RUN -> // 运行
                        QuartzScheduleUtil.runScheduleJob(scheduler, scheduleJob)
                    ScheduleJobOperationTypeEnum.PAUSE -> // 暂停
                        QuartzScheduleUtil.pauseScheduleJob(scheduler, scheduleJob)
                    ScheduleJobOperationTypeEnum.RESUME -> // 恢复
                        QuartzScheduleUtil.resumeScheduleJob(scheduler, scheduleJob)
                    ScheduleJobOperationTypeEnum.DELETE -> // 删除
                        QuartzScheduleUtil.deleteScheduleJob(scheduler, scheduleJob)
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------


}

// -----------------------------------------------------------------------------------------------------

// End EventListener class

/* End of file EventListener.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/event/EventListener.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------
