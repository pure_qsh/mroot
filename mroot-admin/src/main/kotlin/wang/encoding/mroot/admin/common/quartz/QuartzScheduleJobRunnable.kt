/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.admin.common.quartz


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.util.ReflectionUtils
import wang.encoding.mroot.common.util.SpringContextUtil

import java.lang.reflect.Method
import java.util.Objects

/**
 * 执行定时任务
 *
 * @author ErYang
 */
class QuartzScheduleJobRunnable
/**
 * 构造方法
 *
 * @param beanName   类
 * @param methodName 方法
 * @param params     参数
 * @throws NoSuchMethodException
 * @throws SecurityException
 */
@Throws(NoSuchMethodException::class, SecurityException::class)
constructor(beanName: String, methodName: String,
            /**
             * 参数
             */
            private val params: String?) : Runnable {

    /**
     * 类
     */
    private val target: Any = Objects.requireNonNull<SpringContextUtil>(SpringContextUtil.getSpringContextUtil()).getBean(beanName)

    /**
     * 方法
     */
    private var method: Method? = null

    init {
        if (null != params && params.isNotBlank()) {
            this.method = target.javaClass.getDeclaredMethod(methodName, String::class.java)
        } else {
            this.method = target.javaClass.getDeclaredMethod(methodName)
        }
    }

    // -------------------------------------------------------------------------------------------------

    companion object {

        private val logger: Logger = LoggerFactory.getLogger(QuartzScheduleJobRunnable::class.java)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 执行
     */
    override fun run() {
        try {
            ReflectionUtils.makeAccessible(method!!)
            if (null != params && params.isNotBlank()) {
                method!!.invoke(target, params)
            } else {
                method!!.invoke(target)
            }
        } catch (e: Exception) {
            if (logger.isErrorEnabled) {
                logger.error(">>>>>>>>执行定时任务失败$e<<<<<<<<")
            }
        }

    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End QuartzScheduleJobRunnable class

/* End of file QuartzScheduleJobRunnable.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/quartz/QuartzScheduleJobRunnable.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

