/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.interceptor


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView
import wang.encoding.mroot.common.exception.BaseException
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * 防止 sql 注入拦截器
 *
 * @author ErYang
 */
@Component
class SqlInjectInterceptor : HandlerInterceptor {

    companion object {

        private val logger: Logger = LoggerFactory.getLogger(SqlInjectInterceptor::class.java)

    }


    @Throws(BaseException::class)
    override fun preHandle(httpServletRequest: HttpServletRequest,
                           httpServletResponse: HttpServletResponse, o: Any): Boolean {
        if (logger.isDebugEnabled) {
            logger.debug(">>>>>>>>SqlInjectInterceptor[${httpServletRequest.requestURI}]<<<<<<<<")
        }
        val names: Enumeration<String> = httpServletRequest.parameterNames
        while (names.hasMoreElements()) {
            val name: String = names.nextElement()
            val values: Array<String> = httpServletRequest.getParameterValues(name)
            for (value: String in values) {
                this.clearXss(value)
            }
        }
        return true
    }

    // -------------------------------------------------------------------------------------------------

    @Throws(BaseException::class)
    override fun postHandle(httpServletRequest: HttpServletRequest,
                            httpServletResponse: HttpServletResponse,
                            o: Any, modelAndView: ModelAndView?) {

    }

    // -------------------------------------------------------------------------------------------------

    @Throws(BaseException::class)
    override fun afterCompletion(httpServletRequest: HttpServletRequest,
                                 httpServletResponse: HttpServletResponse,
                                 o: Any, e: Exception?) {

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理字符转义
     *
     * @param value
     * @return
     */
    private fun clearXss(value: String): String {
        var valueStr: String = value
        if (valueStr.isEmpty()) {
            return valueStr
        }
        valueStr = valueStr.replace("<".toRegex(), "<").replace(">".toRegex(), ">")
        valueStr = valueStr.replace("\\(".toRegex(), "(").replace("\\)", ")")
        valueStr = valueStr.replace("'".toRegex(), "'")
        valueStr = valueStr.replace("eval\\((.*)\\)".toRegex(), "")
        valueStr = valueStr.replace("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']".toRegex(), "\"\"")
        valueStr = valueStr.replace("script", "")
        return valueStr
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SqlInjectInterceptor class

/* End of file SqlInjectInterceptor.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/interceptor/SqlInjectInterceptor.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
